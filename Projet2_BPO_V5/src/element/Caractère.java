package element;

public class Caract�re extends Element {

	private Character c;

	public Caract�re(Character c, int x, int y) {
		super(x, y);
		this.setC(c);
	}

	public Caract�re(Caract�re ch) {
		this(ch.c,ch.getX(),ch.getY());
		this.encadrer(ch.getEncadrement());
	}

	public Character getC() {
		return new Character(c.charValue());
	}

	public void setC(Character c) {
		this.c = new Character(c);
	}

	public void seDessiner(char[][] imgTmp) {
		if (this.pla�able(imgTmp, 0, 0))
			imgTmp[this.getY()][this.getX()] = this.c;
		if (this.estEncadr�())
			this.dessinerEncadrement(imgTmp, 0, 0);
	}

	public boolean pla�able(char[][] imgTmp, int x, int y) {
		return this.getX() < imgTmp[0].length && this.getY() < imgTmp.length && this.getX() >= 0 && this.getY() >= 0;
	}

	public void dessinerEncadrement(char[][] imgTmp, int x, int y) {
		final int larg = 3;
		new Cadre(x-1, y+1, larg, larg, this.getEncadrement()).seDessiner(imgTmp);
	}

	public Element clone() {
		return new Caract�re(this);
	}
}
