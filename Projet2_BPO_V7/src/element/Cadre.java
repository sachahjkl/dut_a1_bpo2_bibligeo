package element;

/**
 * Classe cadre : Un caract�re c � une position (x, y)
 * est rep�t� sur une longueur LenX ou LenY.
 * Il permet de cr�er les lignes qui composent un encadrement
 */
public class Cadre {
	private int x, y, lenX, lenY;
	private Character c;
	
	/** Initialise un cadre avec une position et des longueurs donn�es */
	public Cadre(int x, int y, int lenX, int lenY, Character c) {
		this.x = x;
		this.y = y;
		this.lenX = lenX;
		this.lenY = lenY;
		this.c = new Character(c);
	}
	
	/**
	 * m�thode seDessiner : dessine toutes lignes necessaire � un encadrement.
	 * 
	 * @param imgTmp
	 *            Un tableau � 2 dimensions ou l'on va dessiner l'encardement         
	 */
	public void seDessiner(char[][] imgTmp) {
		new Ligne(x, y, repeat(c, lenX + 1), Direction.Est).seDessiner(imgTmp);
		new Ligne(x + lenX, y - 1, repeat(c, lenY), Direction.Sud).seDessiner(imgTmp);
		new Ligne(x + lenX - 1, y - lenY, repeat(c, lenX), Direction.Ouest).seDessiner(imgTmp);
		new Ligne(x, y, repeat(c, lenY), Direction.Sud).seDessiner(imgTmp);
		new Ligne(x - 1, y + 1, repeat(c, lenX + 3), Direction.Est).seDessiner(imgTmp);
		new Ligne(x + lenX + 1, y, repeat(c, lenY + 2), Direction.Sud).seDessiner(imgTmp);
		new Ligne(x + lenX, y - lenY - 1, repeat(c, lenX + 2), Direction.Ouest).seDessiner(imgTmp);
		new Ligne(x - 1, y + 1, repeat(c, lenY + 2), Direction.Sud).seDessiner(imgTmp);
	}
	/**
	 * @return les coordonn�es des points les plus en haut � gauche et en bas � droite de l'Element
	 * 
	 * @param c
	 *            le caract�re � r�p�ter.
	 * @param len
	 *            La longueur sur laquelle est r�p�t�e le caract�re.
	 *            
	 */
	public static String repeat(char c, int len) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < len; ++i)
			sb.append(c);
		return sb.toString();
	}

}
