package element;

import java.util.ArrayList;
/**
 * Classe Texte : �tend Element
 * 
 * Definie par une liste de caract�re
 */
public class Texte extends ElementCoordonn� {

	private ArrayList<String> txt;
	/**Initialise un texte � partir de ses coordonn�es*/
	public Texte(int x, int y) {
		super(x, y);
		this.txt = new ArrayList<String>();
	}
	/**Initialise un texte � partir d'un texte*/
	public Texte(Texte t) {
		super(t.getX(), t.getY());
		this.txt = new ArrayList<String>(t.txt);
		this.encadrer(t.getEncadrement());
	}
	/**Ajoute une ligne (chaine de caratct�re) dans le texte */
	public void ajouterligneTxt(String strTxt) {
		String[] strLigne = strTxt.split("[\\r\\n]+");
		for (String s : strLigne)
			this.txt.add(s);
	}
	/**
	 * Dessine la chaine de caract�re dans une image avec son encadrement si il est encadr�
	 * 
	 * @param imgTmp
	 *            Un tableau � 2 dimensions ou l'on va dessiner le caract�re           
	 */
	public void seDessiner(char[][] imgTmp) {
		int y = this.getY();
		for (int i = 0; i < this.txt.size(); ++i) {
			int x = this.getX();
			for (char c : txt.get(i).toCharArray()) {
				new Caract�re(c, x, y).seDessiner(imgTmp);
				++x;
			}
			--y;
		}
		if (this.estEncadr�()) {
			new Cadre(this.getX() - 1, this.getY() + 1, this.getLenMax() + 1, this.txt.size() + 1,
					this.getEncadrement()).seDessiner(imgTmp);
		}
	}

	ArrayList<String> getTxt() {
		return new ArrayList<String>(this.txt);
	}
	/**
	 * @return La longueur de la ligne la plus longue que contient le texte           
	 */
	int getLenMax() {
		int lenXMax = 0;
		for (String s : this.txt)
			lenXMax = lenXMax < s.length() ? s.length() : lenXMax;
		return lenXMax;
	}
	/**
	 * @return un clone de l'Element sans sa r�f�rence.
	 */
	public ElementCoordonn� clone() {
		return new Texte(this);
	}

	/**
	 * @return les coordonn�es des points les plus en haut � gauche et en bas � droite de l'Element
	 */
	public int[] getCoordExtreme() {
		if (this.estEncadr�())
			return new int[] { this.getX() - 1, this.getY() + 1, this.getX() + this.getLenMax() + 1,
					this.getY() - this.txt.size() - 1 };
		else
			return new int[] { this.getX(), this.getY(), this.getX() + this.getLenMax(),
					this.getY() - this.txt.size() };
	}

}
