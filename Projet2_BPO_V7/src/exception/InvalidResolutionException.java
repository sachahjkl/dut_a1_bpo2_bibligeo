package exception;
/**
 * Exeption renvoy� si la r�solution d'une image est diff�rente de celle du film
 * 
 */
public class InvalidResolutionException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public String toString() {
		return "Image resolution is different from Film ";
	}

}
